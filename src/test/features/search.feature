Feature: Rechercher une dictée par son titre
  
 

Scenario: Rechercher une dictée par titre
  Given je suis sur la page d accueil
  When je clique sur search 
  And j'inscris "le berger et la mer"
  Then la plateforme doit afficher la liste des dictées correspondant à ce titre
 
import {Given, When, Then, setDefaultTimeout} from "@cucumber/cucumber"
import {chromium, Page, Browser, expect} from "@playwright/test"


setDefaultTimeout(30000);


let browser: Browser;
let page: Page;

Given('je suis sur la page d\'inscription', async function () {
    browser = await chromium.launch({headless: false});
    page = await browser.newPage();
    await page.goto("https://dictable.org/auth/signup/",{ timeout: 50000 })
   expect(await page.locator('text=Inscription')).toBeVisible();
  });


When('je remplis le formulaire d\'inscription avec les données suivantes:', async function (dataTable) {
    const { email, password, confirmPwd } = dataTable.rowsHash();
    await page.fill('#email', email);
    await page.fill('#password', password);
    await page.fill('#re_password', confirmPwd);
   
});  


When('je clique sur le bouton {string}', async function (string) {
   // await page.getByRole('button', { name: 'Inscription' }).click({timeout: 50000}); 
   //await this.page.click('button[type="submit"]:has-text("Inscription")');
   await page.locator('//*[@id=":rh:"]').click({timeout: 50000})
    
  });


Then('je dois recevoir un message m\'invitant à vérifier ma boîte mail', async function () {
   const message = page.locator("p.MuiTypography-root");
   await expect (message).toBeVisible();
    
});  


When('j\'ouvre le site Yopmail', async function () {
    await page.goto("https://yopmail.com/fr/wm")
    expect(await page.locator('text=luisa@yopmail.com')).toBeVisible();
});


When('je recherche le premier message de la boîte de réception', async function () {

   
});


When('j\'ouvre le message et je clique sur le lien de validation', async function () {
   
});


Then('je dois être redirigé vers la page de connexion', async function () {
    await expect(page.url()).toContain('/connexion'); 
});


When('je me connecte avec les identifiants suivants:', async function (dataTable) {
    const { email, password } = dataTable.rowsHash();
    await page.fill('#email', email);
    await page.fill('#password', password);
    await page.click('#btnSeConnecter');
});

When('je clique sur le bouton "Se connecter"', async function () {

   await page.click('#btnSeConnecter');
   expect(await page.locator(" ")).toBeVisible();
});


Then('je dois être redirigé vers la page des paramètres initiaux', async function () {
    expect(page.url()).toContain('/init');
    expect(await page.title()).toBe('Paramètres initiaux');
    
});


When('je remplis les paramètres initiaux avec les données suivantes:', async function (dataTable) {
    const { pseudo, nom, prenom, langue, genre, pays, niveau } = dataTable.rowsHash();

    await page.fill('input[name="pseudo"]', pseudo);
    await page.fill('input[name="firt_name', nom);
    await page.fill('input[name="last_name"]', prenom);
    await page.selectOption('select[name="gender"]', genre);
    await page.selectOption('select[name="language"]', langue);
    await page.selectOption('select[name="academic_la"]', niveau);
    await page.selectOption('#Country', pays);
  
    
});


When('j\'enregistre', async function () {
   await this.page.click('button[type="submit"]:has-text("Enregistrer")'); 
});


Then('je dois être redirigé vers la page d\'accueil', async function () {
    await page.goto("https://dictable.org/profiles",{ timeout: 50000 }) 
    expect(await page.title()).toBe('Qui etes-vous?');
});

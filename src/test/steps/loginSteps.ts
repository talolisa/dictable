import{Given, When, Then, setDefaultTimeout} from "@cucumber/cucumber"
import {chromium, Page, Browser, expect} from "@playwright/test"

setDefaultTimeout(60 * 1000);

let browser: Browser;
    let page: Page;

Given('je suis sur la page de connexion', async function () {
    
    
        browser = await chromium.launch({headless: false});
        page = await browser.newPage();
        await page.goto("https://dictable.org/auth/signin/",{ timeout: 60000 })
        expect(await page.locator('h1.MuiTypography-root').first()).toBeVisible({ timeout: 10000 });
      

  });


  When('j\'entre mes identifiants {string} et {string}', async function (string, string2) {
    await page.fill('#email', 'lionel@yopmail.com');
    await page.fill('#password', '1234@lion');
    //await page.locator('button#":rm:" .MuiButtonBase-root.MuiButton-root.MuiloadingButton-root.MuiButton-containedSizeMedium.MuiButton-fullWidth.css-1c23caj').click({timeout: 50000});
    await page.click('button[type="submit"]');
});
  
  
  Then('je suis redirection à la page d\'accueil', async function () {
    await expect(page).toHaveURL("https://dictable.org/profiles");
  });
  
  
  When('j entre mes identifiants lionel@yopmail.com et 1235@lion', async function () {
    await page.fill("#email", "lionel@yopmail.com");
    await page.fill("#password", "1235@lion");
   // await page.locator('button#":rm:" .MuiButtonBase-root.MuiButton-root.MuiloadingButton-root.MuiButton-containedSizeMedium.MuiButton-fullWidth.css-1c23caj').click({timeout: 50000});
   await page.click('button[type="submit"]');
});

  
  
  Then('je recois un mot de passe incorrect', async function () {
    const errorMessage = await page.textContent("#notistack-snackbar.SnackbarItem-message");
    expect(errorMessage).toContain("Detail: No active account found with the given credentials");
  });


  
  When('j entre mes identifiants lionl@yopmail.com et talo1234', async function () {
    await page.fill("#email", "lionl@yopmail.com");
    await page.fill("#password", "talo1234");
    //await page.locator('button#":rm:" .MuiButtonBase-root.MuiButton-root.MuiloadingButton-root.MuiButton-containedSizeMedium.MuiButton-fullWidth.css-1c23caj').click({timeout: 50000});
    await page.click('button[type="submit"]');
});



  Then('je recois un Aucun compte actif n’a été trouvé avec les informations d’identification fournies', async function () {
/*await page.waitForSelector('#notistack-snackbar .SnackbarItem-message', { timeout: 60000});
  const errorMessage = await page.textContent('#notistack-snackbar .SnackbarItem-message');
  expect(errorMessage).toContain('Detail:No active account found with the given credentials');*/
  const errorMessage = await page.textContent('#notistack-snackbar .SnackbarItem-message');
  expect(errorMessage).toContain('No active account found with the given credentials');
  expect(await page.isVisible('#notistack-snackbar')).toBeTruthy();
  });
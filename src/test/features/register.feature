Feature: Inscription
En tant qu'utilisateur non inscrit Je veux pouvoir m'inscrire sur l'application afin d'accéder à ses fonctionnalités

Scenario: Inscription avec succès

    Given je suis sur la page d'inscription
    When je remplis le formulaire d'inscription avec les données suivantes:
    | email       | nina@yopmail.com |
    | password    | nina1234         |
    | confirmPwd  | nina1234         |
    And je clique sur le bouton "S'inscrire"
    Then je dois recevoir un message m'invitant à vérifier ma boîte mail
    When j'ouvre le site Yopmail
    And je recherche le premier message de la boîte de réception
    And j'ouvre le message et je clique sur le lien de validation
    Then je dois être redirigé vers la page de connexion
    When je me connecte avec les identifiants suivants:
    | email       | nina@yopmail.com |
    | password    | nina1234         |
    And j'enregistre 
    Then je dois être redirigé vers la page des paramètres initiaux
    When je remplis les paramètres initiaux avec les données suivantes:
    | pseudo      | testUser        |
    | nom         | nina           |
    | prenom      | nina          |
    | langue      | Français        |
    | genre       | Feminin         |
    | pays        | Cameroun        |
    | niveau      | Université      |
    And je clique sur le bouton "Terminer"
    Then je dois être redirigé vers la page d'accueil
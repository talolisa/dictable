import { Given, When, Then, setDefaultTimeout} from "@cucumber/cucumber";
import {chromium, Page, Browser, expect} from "@playwright/test"

setDefaultTimeout(60 * 1000);

let browser: Browser;
    let page: Page;


Given('je suis sur la page d accueil', async function () {
    browser = await chromium.launch({headless: false});
    page = await browser.newPage();
    await page.goto("https://dictable.org/auth/signin/",{ timeout: 60000 })
    expect(await page.locator('h1.MuiTypography-root').first()).toBeVisible({ timeout: 10000 });
    await page.fill('#email', 'lionel@yopmail.com');
    await page.fill('#password', '1234@lion');
    await page.click('button[type="submit"]');
    await expect(page).toHaveURL("https://dictable.org/profiles");
    await page.locator('div').filter({ hasText: /^BB$/ }).click({ timeout: 60000 });
    
  });
   

  When('je clique sur search', async function () {
    await page.getByLabel('search').click();
    await page.getByPlaceholder('Rechercher dans Dictable').click();

  });
  
  When('j\'inscris {string}', async function (string) {
    await page.getByPlaceholder('Rechercher dans Dictable').fill('le chameau et zeus');
    await page.getByText('Le Chameau et Zeus').click();
  }); 

  Then('la plateforme doit afficher la liste des dictées correspondant à ce titre', async function () {
   
  });
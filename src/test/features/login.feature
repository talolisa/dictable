Feature: Se connecter à l'application

    Background:
        Given je suis sur la page de connexion
  
  
    Scenario: se connecter avec succès
        When j'entre mes identifiants "lionel@yopmail.com" et "1234@lion"
        Then je suis redirection à la page d'accueil

        
    
    Scenario Outline: se connecter avec echec 
        When j entre mes identifiants <Adresse email> et <Mot de passe>
        Then  je recois un <errorMessage>
        Examples: 
        | Adresse email       | Mot de passe | errorMessage                                                                      | 
        | lionel@yopmail.com  | 1235@lion    | mot de passe incorrect                                                            |
        | lionl@yopmail.com   | talo1234     | Aucun compte actif n’a été trouvé avec les informations d’identification fournies |
        